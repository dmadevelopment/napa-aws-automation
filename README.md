<h1>Add a client to IPM</h1>
<h4>This project uses the AWS SDK for java, aiming to automate the addition of a new EC2 instance, running the IPM application, with all the needed infrastructure.</h4>

The intended usage is to run Main#main, where all the other functionality will be called in sequence. Search the output for the ssh command to connect to the instance, for example search for "ssh -o".

Functionalities implemented so far:
<ul>
    <li>create a security group with ports 22 and 8000 open</li>
    <li>create a subnet</li>
    <li>launch a t2 micro instance</li>
    <li>terminate the instance, delete the security group and the subnet, using the TearDown class</li>
    <li>start up the IPM application on the EC2 instance</li>
</ul>

Remains to do:
<ul>
    <li>test the Route53 code and add it to the main class</li>
    <li>(probably) create an IAM user with access rights to the instance and the subnet</li>
</ul>
#!/bin/bash
yum update
yum -y install postgresql-server
postgresql-setup initdb
systemctl enable postgresql.service
systemctl start postgresql.service
sudo -u postgres psql postgres -c "create role napa superuser;"
sudo -u postgres psql postgres -c "alter role napa with LOGIN;"
sudo -u postgres psql postgres -c "create database napa;"
amazon-linux-extras install java-openjdk11
awk '{if($1=="host") print substr($0, 1, length($0)-length($NF)) "trust"; else print $0}' /var/lib/pgsql/data/pg_hba.conf > /var/lib/pgsql/data/pg_hba_customghb.conf
rm /var/lib/pgsql/data/pg_hba.conf
mv /var/lib/pgsql/data/pg_hba_customghb.conf /var/lib/pgsql/data/pg_hba.conf
systemctl restart postgresql.service
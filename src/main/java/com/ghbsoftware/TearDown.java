package com.ghbsoftware;

import lombok.Builder;
import lombok.CustomLog;
import lombok.NonNull;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.DeleteSecurityGroupRequest;
import software.amazon.awssdk.services.ec2.model.DeleteSubnetRequest;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.InstanceState;
import software.amazon.awssdk.services.ec2.model.InstanceStateName;
import software.amazon.awssdk.services.ec2.model.Subnet;
import software.amazon.awssdk.services.ec2.model.Tag;
import software.amazon.awssdk.services.ec2.model.TerminateInstancesRequest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class can be used to terminate an EC2 instance, and delete the
 * associated subnet and security group. They should all have the same name.
 * A main class is provided for convenience. (And because of manual testing).
 * <p>
 * Example usage:
 * <pre>
 *   var tearDown = new TearDown(name, ec2);
 *   tearDown.stopEc2InstanceByName();
 *   tearDown.deleteSecurityGroup();
 *   tearDown.deleteSubnet();
 *   </pre>
 */
@CustomLog
@Builder
public class TearDown {
    public static final String RUNNING_STATE = getRunningStateAsString();
    @NonNull
    private final String name;
    private final Ec2Client ec2;

    private static String getRunningStateAsString() {
        return InstanceState.builder()
                .name(InstanceStateName.RUNNING)
                .build()
                .nameAsString();
    }

    public static void main(String[] args) throws IOException {
        String name = args[0];
        try (var ec2 = Ec2Client.builder().build()) {
            var tearDown = new TearDown(name, ec2);
            terminateEc2(tearDown);
            deleteSecurityGroup(tearDown);
            deleteSubnet(tearDown);
            tearDown.deleteKnownHostsFile();
            // maybe delete Route53 record as well
        }
    }

    private void deleteKnownHostsFile() throws IOException {
        Files.deleteIfExists(Path.of(name + "known_hosts"));
        log.info("deleted known hosts file if it existed");
    }

    private static void deleteSubnet(TearDown tearDown) {
        try {
            tearDown.deleteSubnet();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private static void deleteSecurityGroup(TearDown tearDown) {
        try {
            tearDown.deleteSecurityGroup();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private static void terminateEc2(TearDown tearDown) {
        try {
            tearDown.terminateEc2InstanceByName();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Terminate the EC2 instance with the given {@link TearDown#name}
     */
    public void terminateEc2InstanceByName() {
        List<String> instanceIds = findInstanceIdByName();
        verifyThereIsOnlyOne(instanceIds, "EC2 instance");
        ec2.terminateInstances(terminateInstanceRequest(instanceIds));
        log.info("terminated ec2 instance {}", name);
    }

    private TerminateInstancesRequest terminateInstanceRequest(
            List<String> instanceIds) {
        return TerminateInstancesRequest.builder()
                .instanceIds(instanceIds)
                .build();
    }

    private List<String> findInstanceIdByName() {
        return ec2.describeInstances()
                .reservations()
                .stream()
                .flatMap(r -> r.instances().stream())
                .filter(i -> i.tags().contains(nameTag()))
                .filter(i -> i.state().nameAsString().equals(RUNNING_STATE))
                .map(Instance::instanceId)
                .collect(Collectors.toList());
    }

    private Tag nameTag() {
        return Main.nameTagOf(name);
    }

    private void verifyThereIsOnlyOne(List<String> items,
                                      final String itemDescription) {
        if (items.size() == 1) {
            return;
        }
        log.error("Not exactly one {} found with the given name. {}s: {}",
                itemDescription, itemDescription, items);
        throw new IllegalStateException(itemDescription + " couldn't be" +
                " identified");
    }

    /**
     * Delete the security group with the given {@link TearDown#name}
     */
    public void deleteSecurityGroup() {
        ec2.deleteSecurityGroup(deleteSecurityGroupRequest());
        log.info("deleted security group {}", name);
    }

    private DeleteSecurityGroupRequest deleteSecurityGroupRequest() {
        return DeleteSecurityGroupRequest.builder()
                .groupName(name)
                .build();
    }

    /**
     * Delete the subnet with the given {@link TearDown#name}
     */
    public void deleteSubnet() {
        String subnetId = findSubnetByName();
        ec2.deleteSubnet(deleteSubnetRequest(subnetId));
        log.info("deleted subnet {}", name);
    }

    private String findSubnetByName() {
        List<String> subnets = ec2.describeSubnets()
                .subnets()
                .stream()
                .filter(s -> s.tags().contains(nameTag()))
                .map(Subnet::subnetId)
                .collect(Collectors.toList());
        verifyThereIsOnlyOne(subnets, "Subnet");
        return subnets.get(0);
    }

    private DeleteSubnetRequest deleteSubnetRequest(String subnetId) {
        return DeleteSubnetRequest.builder()
                .subnetId(subnetId)
                .build();
    }
}

package com.ghbsoftware;

import lombok.Builder;
import lombok.CustomLog;
import lombok.NonNull;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.AttributeBooleanValue;
import software.amazon.awssdk.services.ec2.model.CreateSubnetRequest;
import software.amazon.awssdk.services.ec2.model.ModifySubnetAttributeRequest;
import software.amazon.awssdk.services.ec2.model.ResourceType;
import software.amazon.awssdk.services.ec2.model.TagSpecification;

import java.util.Arrays;

/**
 * This class can be used to create a subnet. Use {@link Main} instead, to
 * create it with the EC2 instance and the security group in one go.
 * A main class is provided for convenience. (And because of manual testing)
 * <p>
 * Example usage:
 * <pre>new SubnetCreator(vpcId, cidrBlock, name).createSubnet();</pre>
 */
@CustomLog
@Builder
public class SubnetCreator {
    private static final String USAGE = "Usage: SubnetCreator vpcId cidrBlock" +
            " name";
    @NonNull
    private final String vpcId;
    @NonNull
    private final String cidrBlock;
    @NonNull
    private final String name;

    // example args: vpc-f4bf2e9e 172.31.255.0/24 johnny
    public static void main(String[] args) {
        log.info("args={}", Arrays.toString(args));
        if (args.length != 3) {
            log.info(USAGE);
            throw new IllegalStateException(USAGE);
        }
        String vpcId = args[0];
        String cidrBlock = args[1];
        String name = args[2];

        var subnetCreator = new SubnetCreator(vpcId, cidrBlock, name);
        subnetCreator.createSubnet();
    }

    String createSubnet() {
        try (Ec2Client ec2 = Ec2Client.builder().build()) {
            String subnetId = ec2.createSubnet(createSubnetRequest())
                    .subnet()
                    .subnetId();
            log.info("created subnet {}", subnetId);
            modifySubnetAutoAssignPublicIpv4(ec2, subnetId, Boolean.TRUE);
            return subnetId;
        }
    }

    public void modifySubnetAutoAssignPublicIpv4(Ec2Client ec2, String subnetId, Boolean shouldAssignPublicIp) {
        ModifySubnetAttributeRequest autoAssignPublicIp =
                buildAssignPublicIpRequest(subnetId, shouldAssignPublicIp);
        ec2.modifySubnetAttribute(autoAssignPublicIp);
    }

    private ModifySubnetAttributeRequest buildAssignPublicIpRequest(String subnetId, boolean shouldAssignPublicIp) {
        var attributeBooleanValue =
                buildAttributeBooleanValue(shouldAssignPublicIp);
        return ModifySubnetAttributeRequest.builder()
                .subnetId(subnetId)
                .mapPublicIpOnLaunch(attributeBooleanValue)
                .build();
    }

    private AttributeBooleanValue buildAttributeBooleanValue(boolean b) {
        return AttributeBooleanValue.builder()
                .value(b)
                .build();
    }

    private CreateSubnetRequest createSubnetRequest() {
        return CreateSubnetRequest.builder()
                .vpcId(vpcId)
                .cidrBlock(cidrBlock)
                .tagSpecifications(getTagForName())
                .build();
    }

    private TagSpecification getTagForName() {
        return TagSpecification.builder()
                .tags(Main.nameTagOf(name))
                .resourceType(ResourceType.SUBNET)
                .build();
    }
}

package com.ghbsoftware;

import lombok.CustomLog;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.Subnet;
import software.amazon.awssdk.services.ec2.model.Tag;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Main entry point of the project. Call the main method of this class with the
 * required arguments to launch an EC2 instance with its own subnet and security
 * group, public IP, running the napa application.
 * <p>
 * Example usage:
 * <pre>Main johnny vpc-f4bf2e9e ami-043097594a7df80ec 173.31.0.0/16
 * /path/to/napa.jar</pre>
 */
@CustomLog
public class Main {
    private static final String USAGE =
            "Usage: Main ec2InstanceName vpcId amiId cidrBlock pathToNapaJar";
    private static String cidrBlock;
    private static String name;
    private static SubnetCreator subnetCreator;

    /**
     * Note before executing:
     * I have my ~/.aws/config and ~/.aws/credentials files set up with the
     * default region and my IAM user credentials respectively. The user has
     * admin access, but less permissions is probably enough.
     * <p>
     * Example usage: Main johnny vpc-f4bf2e9e ami-043097594a7df80ec 172.31.255.0/24
     * path/to/napa.jar
     *
     * @param args name vpcId amiId cidrBlock - the provided name parameter will be
     *             the name of the EC2 instance, the security group and also the subnet.
     */
    public static void main(String[] args) {
        validateArgsLength(args);
        name = args[0];
        String vpcId = args[1];
        String amiId = args[2];
        cidrBlock = args[3];
        String pathToNapaJar = args[4];

        SecurityGroupCreator group = createSecurityGroup(vpcId);
        String subnetId = createSubnet(group);
        launchEc2(amiId, group, pathToNapaJar, subnetId);
    }

    private static SecurityGroupCreator createSecurityGroup(String vpcId) {
        var group = new SecurityGroupCreator(name, vpcId);
        group.createSecurityGroup();
        return group;
    }

    private static void validateArgsLength(String[] args) {
        if (args.length != 5) {
            log.error(USAGE);
            throw new IllegalStateException(USAGE);
        }
    }

    private static void launchEc2(String amiId, SecurityGroupCreator group,
                                  String pathToNapaJar, String subnetId) {
        var ec2Launcher = Ec2Launcher.builder()
                .instanceName(group.getEc2InstanceName())
                .amiId(amiId)
                .securityGroupId(group.getGroupId())
                .subnetId(subnetId)
                .pathToNapaJar(pathToNapaJar)
                .build();
        ec2Launcher.startEc2Instance();
        doNotAssignPublicIps(subnetId);
    }

    private static void doNotAssignPublicIps(String subnetId) {
        try (Ec2Client ec2 = Ec2Client.builder().build()) {
            subnetCreator.modifySubnetAutoAssignPublicIpv4(ec2, subnetId,
                    Boolean.FALSE);
        }
    }

    private static String createSubnet(SecurityGroupCreator group) {
        String vpcId = group.getVpcId();
        subnetCreator = new SubnetCreator(vpcId, cidrBlock,
                group.getEc2InstanceName());
        String subnetId = subnetCreator.createSubnet();
        waitForSubnetAttributeChange();
        return subnetId;
    }

    @SuppressWarnings("java:S2142") // should re-interrupt thread
    private static void waitForSubnetAttributeChange() {
        var errorMessage = "Could not modify subnet attribute";
        try (Ec2Client ec2 = Ec2Client.builder().build()) {
            waitForChange(errorMessage, ec2);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IllegalStateException(errorMessage);
        }
    }

    private static void waitForChange(String errorMessage, Ec2Client ec2) throws InterruptedException {
        var executorService =
                checkSubnetAttribute(ec2);
        if (!executorService.awaitTermination(30, TimeUnit.SECONDS)) {
            throw new IllegalStateException(errorMessage);
        }
    }

    private static ScheduledExecutorService checkSubnetAttribute(Ec2Client ec2) {
        var scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor();
        Runnable publicIpWillBeAssigned = getAttributeChecker(ec2,
                scheduledExecutorService);
        scheduledExecutorService.scheduleAtFixedRate(publicIpWillBeAssigned,
                2, 5, TimeUnit.SECONDS);
        return scheduledExecutorService;
    }

    private static Runnable getAttributeChecker(Ec2Client ec2, ScheduledExecutorService scheduledExecutorService) {
        return () ->  {
            if (willPublicIpBeAssigned(ec2)) {
                scheduledExecutorService.shutdownNow();
            }
        };
    }

    private static boolean willPublicIpBeAssigned(Ec2Client ec2) {
        return ec2
                .describeSubnets()
                .subnets()
                .stream()
                .filter(Subnet::mapPublicIpOnLaunch)
                .anyMatch(s -> s.tags().contains(nameTagOf(name)));
    }

    public static Tag nameTagOf(String name) {
        return Tag.builder()
                .key("Name")
                .value(name)
                .build();
    }
}

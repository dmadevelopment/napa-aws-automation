package com.ghbsoftware;

import lombok.CustomLog;
import software.amazon.awssdk.services.route53.Route53Client;
import software.amazon.awssdk.services.route53.model.Change;
import software.amazon.awssdk.services.route53.model.ChangeAction;
import software.amazon.awssdk.services.route53.model.ChangeBatch;
import software.amazon.awssdk.services.route53.model.ChangeResourceRecordSetsRequest;
import software.amazon.awssdk.services.route53.model.RRType;
import software.amazon.awssdk.services.route53.model.ResourceRecord;
import software.amazon.awssdk.services.route53.model.ResourceRecordSet;

/**
 * This class can be used to create an A record for the DNS, mapping the
 * given domain name to an IP address.
 * <p>
 * FUNCTIONALITY NOT TESTED AT ALL
 */
@CustomLog
public class Route53 {
    public static void main(String[] args) {
        String zoneId = args[0];
        String domainName = args[1];
        String publicIpv4 = args[2];
        Route53Client route53 = Route53Client.builder()
                .build();
        var resourceRecord = ResourceRecord.builder()
                .value(publicIpv4)
                .build();
        var records = ResourceRecordSet.builder()
                .name(domainName)
                .type(RRType.A)
                .resourceRecords(resourceRecord)
                .build();
        var change = Change.builder()
                .action(ChangeAction.CREATE)
                .resourceRecordSet(records)
                .build();
        var changeBatch = ChangeBatch.builder()
                .changes(change)
                .build();
        var request =
                ChangeResourceRecordSetsRequest.builder()
                        .hostedZoneId(zoneId)
                        .changeBatch(changeBatch)
                        .build();
        var response = route53.changeResourceRecordSets(request);
        log.info("route 53 change response: {}",
                response.changeInfo().status());
    }
}

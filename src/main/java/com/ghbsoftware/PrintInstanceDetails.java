package com.ghbsoftware;

import lombok.Builder;
import lombok.CustomLog;
import lombok.NonNull;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.Tag;

import java.util.List;

/**
 * Utility class to log the public IPv4 and ARN of all of your EC2 instances.
 */
@Builder
@CustomLog
public class PrintInstanceDetails {
    private static final String USAGE = "Usage: PrintInstanceDetails accountName";

    @NonNull
    private final String accountName;

    public static void main(String[] args) {
        if (args.length != 1) {
            log.error(USAGE);
        }
        String accountName = args[0];
        new PrintInstanceDetails(accountName).printInstancesInfo();
    }

    /**
     * Prints the IPv4 and ARN of all of your instances that have any,
     * with the configured account name in the ARN.
     */
    public void printInstancesInfo() {
        try (Ec2Client ec2 = Ec2Client.builder().build()) {
            ec2.describeInstances()
                    .reservations()
                    .stream()
                    .flatMap(r -> r.instances().stream())
                    .forEach(this::printDetails);
        }
    }

    /**
     * prints the ipv4 and ARN of the instance with the provided account name
     * @param i ec2 instance
     */
    public void printDetails(Instance i) {
        String ip = i.publicIpAddress();
        List<Tag> name = i.tags();
        if (ip == null) return;
        log.info("ip address of {}: {}", name, ip);
        String region = i.publicDnsName().split("\\.")[1];
        String arn = "arn:aws:ec2:" + region + ":" + accountName
                + ":instance/" + i.instanceId();
        log.info("arn of {}: {}", name, arn);
    }

    /**
     * Print IPv4 and ARN with the account name YOUR_ACCOUNT_NAME_HERE
     * @param i ec2 instance
     */
    public static void printDetailsOf(Instance i) {
        new PrintInstanceDetails("YOUR_ACCOUNT_NAME_HERE")
                .printDetails(i);
    }
}

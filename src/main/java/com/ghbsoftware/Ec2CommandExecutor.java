package com.ghbsoftware;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import lombok.Builder;
import lombok.CustomLog;
import lombok.NonNull;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Copies napa.jar over to the EC2 instance.
 * <p>
 * Example usage:
 * <pre>JarCopier.builder()
 *       .pathToNapaJar(pathToNapaJar)
 *       .pathToPemKey(pathToPemKey)
 *       .publicIp(publicIp)
 *       .build()
 *       .runCommand();</pre>
 */
@CustomLog
public class Ec2CommandExecutor {
    private final String pathToNapaJar;
    private final String pathToPemKey;
    private final String publicIp;
    private String pathToJarOnEc2;
    private String knownHostsOption;
    private String knownHostsFileLocation;

    @Builder
    public Ec2CommandExecutor(@NonNull String pathToNapaJar,
                              @NonNull String pathToPemKey,
                              @NonNull String publicIp) {
        this.pathToNapaJar = pathToNapaJar;
        this.pathToPemKey = pathToPemKey;
        this.publicIp = publicIp;
        readApplicationAttributes();
    }

    public static void main(String[] args) {
        String pathToNapaJar = args[0];
        String pathToPemKey = args[1];
        String publicIp = args[2];

        Ec2CommandExecutor ec2CommandExecutor = Ec2CommandExecutor.builder()
                .pathToNapaJar(pathToNapaJar)
                .pathToPemKey(pathToPemKey)
                .publicIp(publicIp)
                .build();
        ec2CommandExecutor.runNapa();
    }

    private void readApplicationAttributes() {
        var yaml = new Yaml();
        InputStream in = getClass().getClassLoader()
                .getResourceAsStream("application.yml");
        @SuppressWarnings("unchecked")
        Map<String, Object> map = (Map<String, Object>) yaml.load(in);
        pathToJarOnEc2 = (String) map.get("jar-location-on-ec2");
        log.info("jar location on ec2: {}", pathToJarOnEc2);
    }

    public void runNapa() {
        tryCreatingKnownHostsFile();
        copyJarOver();
        runApplication();
    }

    @SuppressWarnings("java:S2142") // should re-interrupt thread
    private void tryCreatingKnownHostsFile() {
        try {
            log.info("created known_hosts file {}", createKnownHostsFile());
        } catch (InterruptedException e) {
            log.error("Could not create known hosts file: {}", e.getMessage());
            throw new IllegalStateException("Could not create known hosts file");
        }
    }

    private String createKnownHostsFile() throws InterruptedException {
        var knownHostsFile = getKnownHostsFilePath();
        var sshKeyScanCommand = String.format("ssh-keyscan -H %s",
                publicIp);
        writeKnownHostsToFile(knownHostsFile, sshKeyScanCommand);
        knownHostsOption = String.format("-o UserKnownHostsFile=%s",
                knownHostsFileLocation);
        return knownHostsFileLocation;
    }

    private void writeKnownHostsToFile(Path knownHostsFile, String
            sshKeyScanCommand) throws InterruptedException {
        var executor = Executors
                .newSingleThreadScheduledExecutor();
        Runnable scanKeys = getKeyScanner(knownHostsFile, sshKeyScanCommand,
                executor);
        executor.scheduleWithFixedDelay(scanKeys, 1, 1,
                TimeUnit.SECONDS);
        if (!executor.awaitTermination(30, TimeUnit.SECONDS)) {
            throw new IllegalStateException("Couldn't get public keys");
        }
    }

    private Runnable getKeyScanner(Path knownHostsFile, String sshKeyScanCommand,
                                   ScheduledExecutorService executor) {
        return () -> {
            String o = tryGettingOutputOfCommand(sshKeyScanCommand);
            if (!o.isBlank()) {
                knownHostsFileLocation = tryWritingToFile(knownHostsFile, o);
                executor.shutdownNow();
            }
        };
    }

    private String tryWritingToFile(Path knownHostsFile, String o) {
        try {
            return Files.writeString(knownHostsFile, o)
                    .toAbsolutePath()
                    .toString();
        } catch (IOException e) {
            log.error("couldn't write to file {}", e.getMessage());
            throw new IllegalStateException("Couldn't write to file");
        }
    }

    @SuppressWarnings("java:S2142") // should re-interrupt thread
    private String tryGettingOutputOfCommand(String command) {
        try {
            return getStandardOutputOfCommand(command);
        } catch (IOException | InterruptedException e) {
            log.error("Could not create known hosts file: {}", e.getMessage());
            throw new IllegalStateException("Could not create known hosts file");
        }
    }

    private String getStandardOutputOfCommand(String sshKeyScanCommand) throws IOException, InterruptedException {
        Process p = Runtime.getRuntime()
                .exec(sshKeyScanCommand);
        var in = new InputStreamReader(p.getInputStream());
        String output = new BufferedReader(in).lines()
                .collect(Collectors.joining("\n"));
        log.info("ssh-keyscan exited with {}", p.waitFor());
        return output;
    }

    private Path getKnownHostsFilePath() {
        String name = Path.of(pathToPemKey)
                .getFileName()
                .toString()
                .split("\\.")[0];
        return Path.of(name + "known_hosts");
    }

    private void copyJarOver() {
        var command = getCopyJarCommand();
        log.info("copying jar:\n{}", command);
        var runtime = Runtime.getRuntime();
        try {
            var process = runtime.exec(command);
            logOutputOfProcess(process);
        } catch (IOException e) {
            log.error("Couldn't execute command {}: {}", command, e);
        }
    }

    private void logOutputOfProcess(Process process) {
        String standardOutput = getOutputOf(process.getInputStream());
        String errorOutput = getOutputOf(process.getErrorStream());
        log.info("Output of command:{}{}", standardOutput, errorOutput);
    }

    private String getOutputOf(InputStream inputStream) {
        var in = new InputStreamReader(inputStream);
        var reader = new BufferedReader(in);
        return reader.lines()
                .collect(Collectors.joining("\n"));
    }

    private String getCopyJarCommand() {
        return String.format("scp %s -i %s %s ec2-user@%s:%s", knownHostsOption,
                pathToPemKey, pathToNapaJar, publicIp, pathToJarOnEc2);
    }

    private String getSshCommand() {
        return String.format("ssh %s -i %s ec2-user@%s", knownHostsOption,
                pathToPemKey, publicIp);
    }

    @SuppressWarnings("java:S2142") // should rethrow interruption
    private void runApplication() {
        try {
            log.info("starting application");
            log.info("ssh command:\n{}", getSshCommand());
            startNapa();
            log.info("visit {}:8000/register", publicIp);
        } catch (JSchException | IOException e) {
            e.printStackTrace();
            log.error("Couldn't start application on ec2 instance: {}",
                    e.getMessage());
        }
    }

    private void startNapa() throws JSchException, IOException {
        var jsch = new JSch();
        jsch.addIdentity(pathToPemKey);
        jsch.setKnownHosts(knownHostsFileLocation);
        var session = jsch.getSession("ec2-user", publicIp,
                22);
        session.connect();
        var channel = session.openChannel("exec");
        var startCommand = String.format("java -jar %s &",
                pathToJarOnEc2);
        log.info("trying to run command:\n{}", pathToJarOnEc2);
        ((ChannelExec)channel).setCommand(startCommand);

        channel.setInputStream(null);
        ((ChannelExec)channel).setErrStream(System.err);
        InputStream in=channel.getInputStream();
        channel.connect();
        var tmp=new byte[1024];
        var stop = false;
        while(!stop){
            while(!stop && in.available()>0){
                int i=in.read(tmp, 0, 1024);
                if(i<0)break;
                var line = new String(tmp, 0, i);
                log.info(line);
                if (line.contains("Started Application in")) {
                    channel.disconnect();
                    session.disconnect();
                    stop = true;
                }
            }
            if(channel.isClosed()){
                if(in.available()>0) continue;
                log.info("exit-status: "+channel.getExitStatus());
                break;
            }
            try{Thread.sleep(1000);}catch(Exception ee){}
        }
        channel.disconnect();
        session.disconnect();
    }
}

package com.ghbsoftware;

import lombok.Builder;
import lombok.CustomLog;
import lombok.Getter;
import lombok.NonNull;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import software.amazon.awssdk.services.ec2.model.CreateSecurityGroupRequest;
import software.amazon.awssdk.services.ec2.model.CreateSecurityGroupResponse;
import software.amazon.awssdk.services.ec2.model.IpPermission;
import software.amazon.awssdk.services.ec2.model.IpRange;
import software.amazon.awssdk.services.ec2.model.ResourceType;
import software.amazon.awssdk.services.ec2.model.TagSpecification;

/**
 * This class can be used to create a security group. Use {@link Main} instead,
 * to create it with the EC2 instance and the subnet in one go.
 *
 * Example usage:
 * <pre>new SecurityGroupCreator(name, vpcId)
 *     .createSecurityGroup();</pre>
 */
@CustomLog
public class SecurityGroupCreator {
    private static final String USAGE = "Usage: SecurityGroupCreator" +
            " <ec2-instance-name> <vpc-id>";

    public String getEc2InstanceName() {
        return ec2InstanceName;
    }

    private final String ec2InstanceName;
    private final String groupName;
    @Getter
    private final String vpcId;
    @Getter
    private String groupId;

    @Builder
    public SecurityGroupCreator(@NonNull String ec2InstanceName,
                                @NonNull String vpcId) {
        this.ec2InstanceName = ec2InstanceName;
        this.vpcId = vpcId;
        groupName = ec2InstanceName;
    }

    public static void main(String[] args) {
        validateArgs(args);
        String name = args[0];
        String vpcId = args[1];
        new SecurityGroupCreator(name, vpcId).createSecurityGroup();
    }

    public void createSecurityGroup() {
        try (var ec2Client = Ec2Client.builder().build()) {
            var securityGroupResponse = createSecurityGroup(ec2Client);
            addInboundRules(ec2Client);
            log.info("added ingress policy to Security Group {}",
                    groupName);
            groupId = securityGroupResponse.groupId();
            log.info("id of newly created security group: {}", groupId);
        }
    }

    private CreateSecurityGroupResponse createSecurityGroup(Ec2Client ec2Client) {
        return ec2Client.createSecurityGroup(CreateSecurityGroupRequest.builder()
                .groupName(groupName)
                .description(groupName)
                .tagSpecifications(getNameTagSpecification())
                .vpcId(vpcId)
                .build());
    }

    private TagSpecification getNameTagSpecification() {
        return TagSpecification.builder()
                .tags(Main.nameTagOf(groupName))
                .resourceType(ResourceType.SECURITY_GROUP)
                .build();
    }

    private void addInboundRules(Ec2Client ec2Client) {
        var ipRange = IpRange.builder()
                .cidrIp("0.0.0.0/0").build();
        var tcp22 = createTcpIpPermission(ipRange, 22);
        var tcp8000 = createTcpIpPermission(ipRange, 8000);
        AuthorizeSecurityGroupIngressRequest authRequest =
                AuthorizeSecurityGroupIngressRequest.builder()
                        .groupName(groupName)
                        .ipPermissions(tcp22, tcp8000)
                        .build();
        ec2Client.authorizeSecurityGroupIngress(authRequest);
    }

    private static void validateArgs(String[] args) {
        if (args.length < 2) {
            log.error(USAGE);
            throw new IllegalStateException(USAGE);
        }
    }

    private IpPermission createTcpIpPermission(IpRange ipRange, int port) {
        return IpPermission.builder()
                .ipProtocol("tcp")
                .toPort(port)
                .fromPort(port)
                .ipRanges(ipRange)
                .build();
    }
}

package com.ghbsoftware;

import lombok.Builder;
import lombok.CustomLog;
import lombok.NonNull;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.CreateKeyPairRequest;
import software.amazon.awssdk.services.ec2.model.CreateKeyPairResponse;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.InstanceType;
import software.amazon.awssdk.services.ec2.model.ResourceType;
import software.amazon.awssdk.services.ec2.model.RunInstancesRequest;
import software.amazon.awssdk.services.ec2.model.RunInstancesResponse;
import software.amazon.awssdk.services.ec2.model.TagSpecification;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Base64;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class is used to launch an EC2 instance with a configured Name tag. Use
 * {@link Main} instead, if you want to create a security group and a subnet as
 * well in one go.
 * <p>
 * Example usage:
 * <pre>
 * var ec2Launcher = Ec2Launcher.builder()
 *     .instanceName(name)
 *     .amiId(amiId)
 *     .securityGroupId(groupId)
 *     .subnetId(subnetId)
 *     .pathToNapaJar(pathToNapaJar)
 *     .build();
 * ec2Launcher.startEc2Instance();
 * </pre>
 */
@CustomLog
public class Ec2Launcher {
    private static final URL userData = Ec2Launcher.class.getClassLoader().getResource("commandsToRunOnEc2Startup.sh");

    private final String instanceName;
    private final String amiId;
    private final String securityGroupId;
    private final String subnetId;
    private final String pathToNapaJar;
    private String ipv4;
    private String instanceId;
    private String pathToPrivateKey;

    @Builder
    public Ec2Launcher(@NonNull String instanceName, @NonNull String amiId,
                       @NonNull String securityGroupId, @NonNull String subnetId,
                       @NonNull String pathToNapaJar) {
        this.instanceName = instanceName;
        this.amiId = amiId;
        this.securityGroupId = securityGroupId;
        this.subnetId = subnetId;
        this.pathToNapaJar = pathToNapaJar;
    }

    private static String getUserData() {
        try {
            var nonNullUrl = Objects.requireNonNull(userData);
            var userData = Files.readString(Path.of(nonNullUrl.toURI()));
            return Base64.getEncoder()
                    .encodeToString(userData.getBytes(StandardCharsets.UTF_8));
        } catch (IOException | URISyntaxException | NullPointerException e) {
            e.printStackTrace();
            throw new IllegalStateException("Cannot access user data");
        }
    }

    public void startEc2Instance() {
        try (Ec2Client ec2 = Ec2Client.builder().build()) {
            Optional<String> id = createEC2Instance(ec2);
            id.ifPresent(i -> log.info("The EC2 Instance ID is {}", i));
        }
    }

    /**
     * Creates a new T2 micro instance
     *
     * @param ec2 ec2 client
     * @return id of the new EC2 instance
     */
    public Optional<String> createEC2Instance(Ec2Client ec2) {
        var instance = startInstance(ec2);
        log.info("started EC2 Instance {} based on AMI {}",
                instanceId, amiId);
        PrintInstanceDetails.printDetailsOf(instance);
        ipv4 = instance.publicIpAddress();
        log.info("waiting for assignment of a public IP");
        while (ipv4 == null) {
            ipv4 = ec2.describeInstances()
                    .reservations()
                    .stream()
                    .flatMap(r -> r.instances().stream())
                    .filter(i -> i.instanceId().equals(instanceId))
                    .findAny()
                    .map(Instance::publicIpAddress)
                    .orElse(null);
        }
        runNapa();
        return Optional.of(instanceId);
    }

    private void runNapa() {
        Ec2CommandExecutor.builder()
                .pathToNapaJar(pathToNapaJar)
                .pathToPemKey(pathToPrivateKey)
                .publicIp(ipv4)
                .build()
                .runNapa();
    }

    private Instance startInstance(Ec2Client ec2) {
        var runRequest = createRunInstancesRequest(ec2);
        RunInstancesResponse response = ec2.runInstances(runRequest);
        var instance = response.instances().get(0);
        instanceId = instance.instanceId();
        return instance;
    }

    private RunInstancesRequest createRunInstancesRequest(Ec2Client ec2) {
        return RunInstancesRequest.builder()
                .imageId(amiId)
                .instanceType(InstanceType.T2_MICRO)
                .maxCount(1)
                .minCount(1)
                .securityGroupIds(securityGroupId)
                .subnetId(subnetId)
                .userData(getUserData())
                .keyName(getKey(ec2))
                .tagSpecifications(getNameTagSpecification())
                .build();
    }

    private TagSpecification getNameTagSpecification() {
        return TagSpecification.builder()
                .resourceType(ResourceType.INSTANCE)
                .tags(Main.nameTagOf(instanceName))
                .build();
    }

    private String getKey(Ec2Client ec2) {
        if (!keyPairAlreadyExists(ec2)) {
            return createKey(ec2);
        }
        pathToPrivateKey = Path.of(instanceName + ".pem")
                .toAbsolutePath()
                .toString();
        return instanceName;
    }

    private boolean keyPairAlreadyExists(Ec2Client ec2) {
        return ec2.describeKeyPairs()
                .keyPairs()
                .stream()
                .anyMatch(k -> k.keyName().equals(instanceName));
    }

    private String createKey(Ec2Client ec2) {
        CreateKeyPairRequest keyRequest = CreateKeyPairRequest.builder()
                .keyName(instanceName)
                .build();
        CreateKeyPairResponse createKeyResponse = ec2.createKeyPair(keyRequest);
        pathToPrivateKey = trySavingPrivateKey(createKeyResponse);
        return instanceName;
    }

    private String trySavingPrivateKey(CreateKeyPairResponse createKeyResponse) {
        try {
            return savePrivateKey(createKeyResponse);
        } catch (IOException e) {
            log.error("Couldn't save private key: {} {}", e.getMessage(), e.getStackTrace());
            throw new IllegalStateException("Couldn't save private key");
        }
    }

    private String savePrivateKey(CreateKeyPairResponse createKeyResponse)
            throws IOException {
        Path keyFile = Path.of(instanceName + ".pem");
        Files.writeString(keyFile, createKeyResponse.keyMaterial());
        setPermissionsToOwnerRead(keyFile);
        return keyFile.toAbsolutePath()
                .toString();
    }

    private void setPermissionsToOwnerRead(Path keyFile) throws IOException {
        Set<PosixFilePermission> perms = Stream.of(PosixFilePermission.OWNER_READ)
                .collect(Collectors.toSet());
        Files.setPosixFilePermissions(keyFile, perms);
    }
}
